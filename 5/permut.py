from flask import Flask, request
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from json import dumps, loads
from flask.ext.jsonpify import jsonify
import itertools

app = Flask(__name__)
api = Api(app)
		
@app.route("/permutations", methods = ['POST'])	
def Translate():
	print (request.is_json)
	content = request.get_json()
	print (content)
	return permutations(content)
	
def permutations(content):
	number = content["input"]
	
	numbers = number.split(" ")
	count = len(numbers)
	numbers = list(map(int, numbers))
	result = (list(itertools.permutations(numbers)))
	content["input"] = []
	for item in result:
		s = ""
		for i in item:
			s += str(i) + " "
		s = s[:-1]
		content["input"].append(s)
		
	print (content)	
	
	return dumps(content)
	
	
    


if __name__ == '__main__':
     app.run(port=5002)