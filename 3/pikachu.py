import timeit
	
def main():
	to_be_sorted = None
	start_time = timeit.default_timer()
	
	with open('to_be_sorted.txt', 'r') as f:
		lines = f.readlines()
        to_be_sorted = [None]*len(lines)
        for i in range(len(lines)):
			to_be_sorted[i] = int(lines[i])
	
	with open('sorted.txt', 'w') as f:
		for n in range(0, len(to_be_sorted)):
			f.write(str(n) + '\n')
	elapsed = timeit.default_timer() - start_time
	print(elapsed)
			
if __name__ == '__main__':
    main()